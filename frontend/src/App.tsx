import React from 'react';
import './App.css';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link,
} from "react-router-dom";

type ResourcesState = {
  ferrum: Number,
  water: Number,
}

type Building = {
  level: Number,
  constructing: boolean,
  finish: Number,
}

type BuildingState = {
  ferrumMine: Building,
  waterTower: Building,
}

type Research = {
  level: Number,
  constructing: boolean,
  finish: Number,
}

type Researches = {
  ferrumOptimization: Research
}

type PlanetState = {
  resources: ResourcesState,
  buildings: BuildingState,
  research: Researches,
  isLoaded: boolean,
}

class App extends React.Component<{}, PlanetState> {
  constructor(props: any) {
    super(props);
    this.state = {
      resources: {
        ferrum: 0,
        water: 0,
      },
      buildings: {
        ferrumMine: {
          level: 0,
          constructing: false,
          finish: 0
        },
        waterTower: {
          level: 0,
          constructing: false,
          finish: 0
        },
      },
      research: {
        ferrumOptimization: {
          level: 0,
          constructing: false,
          finish: 0
        }
      },
      isLoaded: false,
    };
  }
  getPlanetState(planetId: String) {
    fetch("http://localhost:3000/planet/" + planetId, {
      headers: {
        'accept': 'application/json',
      }
    })
      .then(res => res.json())
      .then(
        (result) => {
          console.info('xxx: %o', result);
          this.setState({
            isLoaded: true,
            resources: {
              water: result.resources.water.value,
              ferrum: result.resources.ferrum.value,
            },
            buildings: {
              ferrumMine: {
                level: result.building_manager.ferrum_mine.level,
                constructing: result.building_manager.ferrum_mine.progress !== "idle",
                finish: 0,
              },
              waterTower: {
                level: result.building_manager.water_tower.level,
                constructing: result.building_manager.water_tower.progress !== "idle",
                finish: 0,
              }
            }
          });
        },
        // Note: it's important to handle errors here
        // instead of a catch() block so that we don't swallow
        // exceptions from actual bugs in components.
        (error) => {
          this.setState({
            isLoaded: true,
          });
        }
      )
  }

  componentDidMount() {
    this.getPlanetState("b7bf3a77-a8d3-4324-b862-a271efc11c11");
    setInterval(() => {
      this.getPlanetState("b7bf3a77-a8d3-4324-b862-a271efc11c11");
    }, 2000);
  }

  render() {
    return <Router>
      <div>
        <Navigation />
        <hr />

        <Resources ferrum={this.state.resources.ferrum} water={this.state.resources.water} />

        {/*
          A <Switch> looks through all its children <Route>
          elements and renders the first one whose path
          matches the current URL. Use a <Switch> any time
          you have multiple routes, but you want only one
          of them to render at a time
        */}
        <Switch>
          <Route exact path="/planet/:planet_id">
            <Planet />
          </Route>
          <Route path="/planet/:planet_id/building">
            <Buildings ferrumMine={this.state.buildings.ferrumMine} waterTower={this.state.buildings.waterTower} />
          </Route>
          <Route path="/planet/:planet_id/research">
            <Research />
          </Route>
        </Switch>
      </div>
    </Router>
  }
}

function Navigation() {
  return (
    <ul>
      <li>
        <Link to="/planet/b7bf3a77-a8d3-4324-b862-a271efc11c11">Planet</Link>
      </li>
      <li>
        <Link to="/planet/b7bf3a77-a8d3-4324-b862-a271efc11c11/building">Building</Link>
      </li>
      <li>
        <Link to="/planet/b7bf3a77-a8d3-4324-b862-a271efc11c11/research">Research</Link>
      </li>
    </ul>
  );
}

class Resources extends React.Component<ResourcesState, {}> {
  render() {
    return <div>
        <h1>Resources</h1>
        <ul>
          <li>Ferrum: {this.props.ferrum}</li>
          <li>Water: {this.props.water}</li>
        </ul>
      </div>
  }
}

function Planet() {
  return (
    <div>
      <h1>Planet</h1>
    </div>
  );
}

class Buildings extends React.Component<BuildingState, {}> {
  render() {
    return <div>
        <h1>Resources</h1>
        <ul>
          <li>Ferrum Mine: 
            <ul>
              <li>Level: {this.props.ferrumMine.level}</li>
              <li>Constructing: {this.props.ferrumMine.constructing}</li>
            </ul>
          </li>
          <li>Water Tower:  <ul>
              <li>Level: {this.props.waterTower.level}</li>
              <li>Constructing: {this.props.waterTower.constructing}</li>
            </ul>
          </li>
        </ul>
      </div>
  }
}

function Research() {
  return (
    <h1>Research</h1>
  );
}

export default App;
