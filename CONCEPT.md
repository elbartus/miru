# MiRu - Mini Rust 

## Idea
I would like to model a simple version of a browser based game like gigra wars in
2 weeks time at the evenings whenever I have time during my vacations.

## Basic structures
Implement
- [xxx] parametrizable speed factor -> got out of scope
- an API to operate on the following types
- a Planet, which contains the subsequent types
- 2 Resources
    - ferrum
    - water
- 3 Buildings, 1 Resource, one Research Building
    - ferrum ore
        - improves ferrum production by a factor
    - water tower
        - improves water production by a factor
    - research center
        - 1 Research Technologies
            - ferrum optimization (factor for ferrum production)
    - ship construction site
        - allows to produce ships
            - 1 Ships types being unlocked by the Technologies
                - devastator
- API operations are
    - buildings/researches:
        - retrieve current level
        - build/research a new level
        - cancel current build/research process
    - ships:
        - build n>=1 ships of type devastator
        - cancel current build order


## Challenges
- If a building or research finishes or any other event occurs,
  we need to compute at that time, what the change is, so that we can compute/
  update the resources at the right point in time. This means, that every event
  in the system, which mutates the state, needs to be processed at the point in time
  when it occurs. This means, we need e.g. a queue of events or Futures with timers, which
  just resolves, when they slept enough.

### Which way to choose

I am really not sure what is better, so I want to write down how I think either way
could work and then I might now more about which method to pick.

There is two:
1. Tokio Futures and using a DelayedQueue
2. Using a GameLoop

### Futures + Delayed Queue
This method would work like this:

There is minimum 2 threads:
- Thread A which handles all events from a DelayedQueue
- Thread B is the webserver, which handles read and write queries from
  the gamer.

Use-Cases:
- A user reads it's planet data:
    
    - Thread B reads from an Arc<RwLock<T>> shared datastructure
      the requested planet data
    - Thread A is not involved at all

- A user starts to construct a building:

    - Thread B verifies, if the user can do this operation -> if yes it 
      it adds an DelayEvent to the DelayQueue. The event gets the duration from when the
      building construction is finished
    - Thread B stores the returned `Key` for cancellation purposes
    - Thread A polls the DelayQueue and gets the event, when the timestamp is reached and processes it
      (removes the Key, mutates the building level)


## Nice resources
- Arc<RwLock<T>>: https://blog.sentry.io/2018/04/05/you-cant-rust-that