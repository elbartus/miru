use chrono::Duration;
use serde_derive::Serialize;
use std::collections::HashMap;
use std::sync::{Arc, RwLock};
use tokio;
use tokio::stream::StreamExt;
use tokio::sync::Mutex;
use tokio::sync::{mpsc, oneshot};
use tokio::time::DelayQueue;
use warp::Filter;
use warp::{Rejection, Reply};

#[derive(Debug)]
pub(crate) enum Command {
    Get {
        key: String,
        resp: oneshot::Sender<Option<String>>,
    },
    Put {
        key: String,
        resp: oneshot::Sender<Option<String>>,
    },
    PutFinished {
        key: String,
    },
}

#[derive(Debug, Serialize)]
struct GetPlanetResponse {
    val: String,
}

pub(crate) fn with_sender(
    tx: Arc<Mutex<mpsc::Sender<Command>>>,
) -> impl Filter<Extract = (Arc<Mutex<mpsc::Sender<Command>>>,), Error = std::convert::Infallible> + Clone
{
    warp::any().map(move || tx.clone())
}

mod handler {
    use super::*;
    pub(crate) async fn get_planet(
        tx: Arc<Mutex<mpsc::Sender<Command>>>,
    ) -> Result<impl Reply, Rejection> {
        let mut tx2 = tx.lock().await.clone();
        let (resp_tx, resp_rx) = oneshot::channel();
        let cmd = Command::Get {
            key: "bla".to_string(),
            resp: resp_tx,
        };
        tx2.send(cmd).await.unwrap();
        let res = resp_rx.await;

        println!("GOT: {:?}", res);

        match res {
            Ok(Some(d)) => Ok(warp::reply::json(&GetPlanetResponse { val: d })),
            _ => Err(warp::reject()),
        }
    }

    pub(crate) async fn construct_building(
        tx: Arc<Mutex<mpsc::Sender<Command>>>,
    ) -> Result<impl Reply, Rejection> {
        let mut tx2 = tx.lock().await.clone();
        let (resp_tx, resp_rx) = oneshot::channel();
        let cmd = Command::Put {
            key: "FerrumOre".to_string(),
            resp: resp_tx,
        };
        tx2.send(cmd).await.unwrap();
        let res = resp_rx.await;

        println!("GOT: {:?}", res);

        match res {
            Ok(Some(d)) => Ok(warp::reply::json(&GetPlanetResponse { val: d })),
            _ => Err(warp::reject()),
        }
    }
}

#[tokio::main]
async fn main() {
    let db: Arc<RwLock<HashMap<String, String>>> = Arc::new(RwLock::new(maplit::hashmap! {
        "blax".to_string() => "HUHU!!!".to_string()
    }));
    let mut qu: DelayQueue<String> = DelayQueue::new();
    let (tx, mut rx) = mpsc::channel(1024);
    let tx2 = tx.clone();
    let mut tx4 = tx.clone();
    let (mut dtx, mut drx) = mpsc::channel(1024);
    let get_planet = warp::path!("planet")
        .and(with_sender(Arc::new(Mutex::new(tx)).clone()))
        .and_then(handler::get_planet);

    let construct_building = warp::path!("planet2")
        .and(warp::post())
        .and(with_sender(Arc::new(Mutex::new(tx2)).clone()))
        .and_then(handler::construct_building);

    let manager = tokio::spawn(async move {
        while let Some(cmd) = rx.recv().await {
            match cmd {
                Command::Get { key, resp } => {
                    resp.send(db.read().unwrap().get(&key).cloned()).unwrap();
                }
                Command::Put { key: _, resp } => {
                    dtx.send("Huhu".to_string()).await.unwrap();
                    resp.send(Some("Planet is building".to_string())).unwrap();
                }
                Command::PutFinished { key } => {
                    println!("FINISHED");
                    db.write().unwrap().insert(key, "blubber".to_string());
                }
            }
        }
    });

    let delay_processor = tokio::spawn(async move {
        loop {
            tokio::select! {
                Some(v) = drx.recv() => {
                    println!("{:?}", v);
                    qu.insert(v, Duration::seconds(2).to_std().unwrap());
                },
                Some(Ok(v)) = qu.next() => {
                    println!("{:?}", v);
                    tx4.send(Command::PutFinished{key: v.into_inner()}).await.unwrap();
                }
            }
        }
    });

    let (_a, _b, _c) = tokio::join!(
        warp::serve(get_planet.or(construct_building)).run(([127, 0, 0, 1], 3030)),
        manager,
        delay_processor,
    );
}
