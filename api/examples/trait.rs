use serde_derive::Serialize;
use std::collections::HashMap;
use strum::VariantNames;
use strum_macros::{EnumString, EnumVariantNames};

#[derive(Debug, PartialEq, Eq, Hash, Serialize, EnumString, Clone, EnumVariantNames)]
#[strum(serialize_all = "snake_case")]
pub enum BuildingClass {
    #[serde(rename(serialize = "ferrum_mine"))]
    FerrumMine,
    #[serde(rename(serialize = "water_tower"))]
    WaterTower,
    #[serde(rename(serialize = "research_center"))]
    ResearchCenter,
    #[serde(rename(serialize = "ship_construction_site"))]
    ShipConstructionSite,
}

#[derive(Debug, PartialEq, Eq, Hash, Serialize, EnumString, Clone, EnumVariantNames)]
#[strum(serialize_all = "snake_case")]
pub enum ResearchClass {
    #[serde(rename(serialize = "ferrum_optimization"))]
    FerrumOptimization,
}

#[derive(Debug, PartialEq, Eq, Hash, Serialize, EnumVariantNames)]
// #[strum(serialize_all = "snake_case")]
pub(crate) enum ItemClass {
    #[serde(rename(serialize = "building"))]
    Building,
    #[serde(rename(serialize = "research"))]
    Research,
}

#[derive(Debug, Serialize)]
pub(crate) struct ItemContainer {
    level: usize,
    class: ItemClass,
}

impl ItemContainer {
    pub(crate) fn new(level: usize, class: ItemClass) -> Self {
        Self { level, class }
    }
}

#[derive(Debug, Serialize)]
struct Manager<T: std::cmp::Eq + std::hash::Hash> {
    inner: HashMap<T, ItemContainer>,
}

impl Manager<ResearchClass> {
    pub(crate) fn new() -> Self {
        let rm = Self {
            inner: maplit::hashmap! {
                ResearchClass::FerrumOptimization => ItemContainer::new(0, ItemClass::Research),
            },
        };

        if rm.inner.len() != ResearchClass::VARIANTS.len() {
            panic!(
                "Did not handle all ResearchClass({}) variants, instead only: {}",
                ResearchClass::VARIANTS.len(),
                rm.inner.len()
            );
        }

        rm
    }
}

impl Manager<BuildingClass> {
    pub(crate) fn new() -> Self {
        let bm = Self {
            inner: maplit::hashmap! {
                BuildingClass::FerrumMine => ItemContainer::new(0, ItemClass::Building),
                BuildingClass::WaterTower => ItemContainer::new(0, ItemClass::Building),
                BuildingClass::ResearchCenter => ItemContainer::new(0, ItemClass::Building),
                BuildingClass::ShipConstructionSite => ItemContainer::new(0, ItemClass::Building),
            },
        };

        if bm.inner.len() != BuildingClass::VARIANTS.len() {
            panic!(
                "Did not handle all BuildingClass({}) variants, instead only: {}",
                BuildingClass::VARIANTS.len(),
                bm.inner.len()
            );
        }

        bm
    }
}

fn main() {
    let bm = Manager::<BuildingClass>::new();
    let rm = Manager::<ResearchClass>::new();

    println!("{:#?}", bm);
    println!("{:#?}", rm);
}
