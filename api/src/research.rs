use crate::ProgressState;
use serde_derive::Serialize;
use std::str::FromStr;
use strum_macros::{EnumString, EnumVariantNames};

#[derive(Debug, PartialEq, Eq, Hash, Serialize, Clone)]
pub struct Research {
    level: usize,
    progress: ProgressState,
}

#[derive(Debug, PartialEq, Eq, Hash, Serialize, EnumString, Clone, EnumVariantNames)]
#[strum(serialize_all = "snake_case")]
pub enum ResearchClass {
    #[serde(rename(serialize = "ferrum_optimization"))]
    FerrumOptimization,
}

impl ResearchClass {
    pub fn from_string(name: String) -> Result<Self, String> {
        ResearchClass::from_str(name.as_str()).map_err(|_| format!("Unknown research class {}", name))
    }
}