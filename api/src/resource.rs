use chrono::Duration;
use serde_derive::Serialize;
use std::collections::HashMap;
use strum::{IntoEnumIterator, VariantNames};
use strum_macros::{EnumIter, EnumString, EnumVariantNames};

#[derive(Debug, PartialEq, Serialize, Clone)]
pub struct Resource {
    pub value: f64,
}

#[derive(Debug, PartialEq, Eq, Hash, Serialize, Clone, EnumIter, EnumString, EnumVariantNames)]
pub enum ResourceClass {
    #[serde(rename(serialize = "ferrum"))]
    Ferrum,
    #[serde(rename(serialize = "water"))]
    Water,
}

impl Resource {
    pub(crate) fn new(value: usize) -> Self {
        Self { value: value as f64 }
    }
}

#[derive(Debug, Serialize, Clone)]
pub(crate) struct ResourceContainer {
    resources: HashMap<ResourceClass, Resource>,
}

impl ResourceContainer {
    pub(crate) fn new() -> Self {
        let rc = Self {
            resources: maplit::hashmap! {
                ResourceClass::Ferrum => Resource::new(0),
                ResourceClass::Water => Resource::new(0)
            },
        };

        if rc.resources.len() != ResourceClass::VARIANTS.len() {
            panic!(
                "Did not handle all ResourceClass({}) variants, instead only: {}",
                ResourceClass::VARIANTS.len(),
                rc.resources.len()
            );
        }

        rc
    }

    pub(crate) fn set_value(&mut self, class: &ResourceClass, value: f64) {
        self.resources.get_mut(&class).unwrap().value = value;
    }

    pub(crate) fn get_value(&self, class: &ResourceClass) -> f64 {
        self.resources.get(&class).unwrap().value
    }
}

impl std::ops::AddAssign for ResourceContainer {
    fn add_assign(&mut self, rhs: Self) {
        let mut res = ResourceContainer::new();
        for class in ResourceClass::iter() {
            res.set_value(&class, self.get_value(&class) + rhs.get_value(&class));
        }

        *self = res
    }
}

pub(crate) trait ResourceProducer {
    fn produce(&self, dur: Duration) -> ResourceContainer;
}
