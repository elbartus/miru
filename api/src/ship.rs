use serde_derive::Serialize;

#[derive(Debug, Serialize, Clone)]
pub struct Ship {
    pub class: ShipClass,
}
#[derive(Debug, Serialize, Clone)]
pub enum ShipClass {
    #[allow(dead_code)]
    Devastator(),
}

impl Ship {
    #[allow(dead_code)]
    fn new(class: ShipClass) -> Self {
        Self { class }
    }
}
