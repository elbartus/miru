use crate::building::BuildingClass;
use crate::planet::{Planet, PlanetId, Planets};
use crate::research::ResearchClass;
use crate::EventId;
use chrono::Duration;
use std::sync::{Arc, RwLock};
use tokio::sync::{mpsc, oneshot};

/// Oneshot responder channel type, used to respond
/// immediate results from the delay manager.
pub(crate) type MiruCmdResponder<T> = oneshot::Sender<T>;

/// Holds the full database, being a RwLock<Planets>
/// This struct needs to be wrapped in a Arc<MiruDb> to share it across
/// tokio tasks / threads.
pub(crate) struct MiruDb {
    pub(crate) inner: RwLock<Planets>,
}

impl MiruDb {
    /// Returns all Planets of the database
    pub(crate) fn get_all(&self) -> Planets {
        self.inner.read().unwrap().clone()
    }

    /// Returns the last update timestamp
    fn get_last_update(&self, planet_id: PlanetId) -> chrono::DateTime<chrono::Utc> {
        self.inner.read().unwrap().get(&planet_id).unwrap().get_last_update()
    }

    /// Refreshes the planet, given a specific elapsed duration
    fn refresh(&self, planet_id: PlanetId, dur: chrono::Duration) -> Result<(), String> {
        self.inner.write().unwrap().get_mut(&planet_id).unwrap().refresh(dur)
    }

    /// Refreshes the planet, given a notion of now() - computes the duration itself
    fn refresh_now(&self, planet_id: PlanetId, now: chrono::DateTime<chrono::Utc>) -> Result<(), String> {
        let lu = self.get_last_update(planet_id);
        let dur = now - lu;
        self.refresh(planet_id, dur)
    }

    /// Returns a specifc planet by `PlanetId`
    fn get_planet(&self, planet_id: PlanetId) -> Option<Planet> {
        self.inner.read().unwrap().get(&planet_id).cloned()
    }

    /// Starts the construction of a building
    fn construct_building(
        &self, planet_id: PlanetId, building_name: String, handle: EventId,
    ) -> Result<EventId, String> {
        self.inner
            .write()
            .unwrap()
            .get_mut(&planet_id)
            .unwrap()
            .building_manager
            .begin_levelling(BuildingClass::from_string(building_name)?, handle)
    }

    /// Finishes the construction of a building
    fn finish_building(&self, planet_id: PlanetId, building_name: String, handle: EventId) -> Result<EventId, String> {
        self.inner
            .write()
            .unwrap()
            .get_mut(&planet_id)
            .unwrap()
            .building_manager
            .finish_levelling(BuildingClass::from_string(building_name)?, handle)
    }

    /// Cancels the construction of a building
    fn cancel_building(&self, planet_id: PlanetId, building_name: String, handle: EventId) -> Result<EventId, String> {
        self.inner
            .write()
            .unwrap()
            .get_mut(&planet_id)
            .unwrap()
            .building_manager
            .cancel_levelling(BuildingClass::from_string(building_name)?, handle)
    }

    /// Starts the research of a technology
    fn start_research(&self, planet_id: PlanetId, research_name: String, handle: EventId) -> Result<EventId, String> {
        self.inner
            .write()
            .unwrap()
            .get_mut(&planet_id)
            .unwrap()
            .resarch_manager
            .begin_levelling(ResearchClass::from_string(research_name)?, handle)
    }

    /// Finishes the research of a technology
    fn finish_research(&self, planet_id: PlanetId, research_name: String, handle: EventId) -> Result<EventId, String> {
        self.inner
            .write()
            .unwrap()
            .get_mut(&planet_id)
            .unwrap()
            .resarch_manager
            .finish_levelling(ResearchClass::from_string(research_name)?, handle)
    }

    /// Cancel a research of a technology
    fn cancel_research(&self, planet_id: PlanetId, research_name: String, handle: EventId) -> Result<EventId, String> {
        self.inner
            .write()
            .unwrap()
            .get_mut(&planet_id)
            .unwrap()
            .resarch_manager
            .cancel_levelling(ResearchClass::from_string(research_name)?, handle)
    }
}

/// Defines Commands to be sent to the Delay Manager
#[derive(Debug)]
pub(crate) enum MiruDelayCmd {
    /// Type to delay the encapsulated `MiruCmd`
    Delay {
        cmd: MiruCmd,
        resp: MiruCmdResponder<Result<EventId, String>>,
        handle: EventId,
        delay: Duration,
    },
    /// Type to cancel the respective `EventId`
    Cancel {
        handle: EventId,
        resp: MiruCmdResponder<Result<(), String>>,
    },
}

/// Defines all Data Manager commands, invoked by the Web Server or
/// the Delay Manager
#[derive(Debug)]
pub(crate) enum MiruCmd {
    /// Get planet information
    GetPlanet {
        planet_id: PlanetId,
        resp: MiruCmdResponder<Option<Planet>>,
    },
    /// Construct a building
    ConstructBuilding {
        planet_id: PlanetId,
        building_name: String,
        resp: MiruCmdResponder<Result<(), String>>,
    },
    /// Finish a building, should only be invoked from the Delay Manager
    FinishBuilding {
        planet_id: PlanetId,
        building_name: String,
        handle: EventId,
    },
    /// Cancel a building construction
    CancelBuilding {
        planet_id: PlanetId,
        building_name: String,
        handle: EventId,
        resp: MiruCmdResponder<Result<(), String>>,
    },
    /// Start a research
    StartResearch {
        planet_id: PlanetId,
        research_name: String,
        resp: MiruCmdResponder<Result<(), String>>,
    },
    /// Finish a research, should only be invoked from the Delay Manager
    FinishResearch {
        planet_id: PlanetId,
        research_name: String,
        handle: EventId,
    },
    /// Cancel a research construction
    CancelResearch {
        planet_id: PlanetId,
        research_name: String,
        handle: EventId,
        resp: MiruCmdResponder<Result<(), String>>,
    },
}

/// The Data Manager is an essential async tokio task, which handles all
/// commands received from the warp webserver handlers. The goal is, that
/// this manager is completely unaware of:
///
/// - web / api specific logic
/// - time specific knowledge in the sense that it should not know anything about now().
///   This means there is only chrono::Duration<chrono::Utc> coming into it.
///   This helps to run everything in the tests easily without the need of advancing time by
///   using thread::sleep() or the like.
pub(crate) async fn run_data_manager(
    db: Arc<MiruDb>, rx: &mut mpsc::Receiver<MiruCmd>, delay_tx: &mut mpsc::Sender<MiruDelayCmd>,
) {
    while let Some(cmd) = rx.recv().await {
        println!("{:?}", cmd);
        match cmd {
            MiruCmd::GetPlanet { planet_id, resp } => {
                db.refresh_now(planet_id, chrono::Utc::now()).unwrap();
                resp.send(db.get_planet(planet_id)).unwrap();
            }
            MiruCmd::ConstructBuilding {
                planet_id,
                building_name,
                resp,
            } => {
                let (htx, hrx) = oneshot::channel();
                let handle = EventId::new_v4();
                db.refresh_now(planet_id, chrono::Utc::now()).unwrap();
                delay_tx
                    .send(MiruDelayCmd::Delay {
                        cmd: MiruCmd::FinishBuilding {
                            planet_id,
                            building_name: building_name.clone(),
                            handle,
                        },
                        resp: htx,
                        handle,
                        delay: Duration::seconds(10),
                    })
                    .await
                    .unwrap();
                let handle = hrx.await.unwrap().unwrap();
                db.construct_building(planet_id, building_name, handle).unwrap();
                resp.send(Ok(())).unwrap();
            }
            MiruCmd::FinishBuilding {
                planet_id,
                building_name,
                handle,
            } => {
                println!("{}, {}, {}", planet_id, building_name, handle);
                db.refresh_now(planet_id, chrono::Utc::now()).unwrap();
                db.finish_building(planet_id, building_name, handle).unwrap();
            }
            MiruCmd::CancelBuilding {
                planet_id,
                building_name,
                handle,
                resp,
            } => {
                let (otx, orx) = oneshot::channel();
                delay_tx.send(MiruDelayCmd::Cancel { handle, resp: otx }).await.unwrap();
                let res = orx.await.unwrap();
                db.refresh_now(planet_id, chrono::Utc::now()).unwrap();
                db.cancel_building(planet_id, building_name, handle).unwrap();
                db.refresh_now(planet_id, chrono::Utc::now()).unwrap();
                resp.send(res).unwrap();
            }
            MiruCmd::StartResearch {
                planet_id,
                research_name,
                resp,
            } => {
                let (htx, hrx) = oneshot::channel();
                let handle = EventId::new_v4();

                db.refresh_now(planet_id, chrono::Utc::now()).unwrap();
                delay_tx
                    .send(MiruDelayCmd::Delay {
                        cmd: MiruCmd::FinishResearch {
                            planet_id,
                            research_name: research_name.clone(),
                            handle,
                        },
                        resp: htx,
                        handle,
                        delay: Duration::seconds(30),
                    })
                    .await
                    .unwrap();
                let handle = hrx.await.unwrap().unwrap();
                db.start_research(planet_id, research_name, handle).unwrap();
                resp.send(Ok(())).unwrap();
            }
            MiruCmd::FinishResearch {
                planet_id,
                research_name,
                handle,
            } => {
                println!("{}, {}, {}", planet_id, research_name, handle);
                db.refresh_now(planet_id, chrono::Utc::now()).unwrap();
                db.finish_research(planet_id, research_name, handle).unwrap();
            }
            MiruCmd::CancelResearch {
                planet_id,
                research_name,
                handle,
                resp,
            } => {
                let (otx, orx) = oneshot::channel();
                delay_tx.send(MiruDelayCmd::Cancel { handle, resp: otx }).await.unwrap();
                let res = orx.await.unwrap();
                db.refresh_now(planet_id, chrono::Utc::now()).unwrap();
                db.cancel_research(planet_id, research_name, handle).unwrap();
                db.refresh_now(planet_id, chrono::Utc::now()).unwrap();
                resp.send(res).unwrap();
            }
        }
    }
}
