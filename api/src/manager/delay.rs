use crate::manager::data::{MiruCmd, MiruDelayCmd};
use std::collections::HashMap;
use std::panic::{self, AssertUnwindSafe};
use tokio::stream::StreamExt;
use tokio::sync::mpsc;
use tokio::time::DelayQueue;

/// Async function, which runs the tokio::time::DelayQueue manager.
/// This manager receives `MiruDelayCmd` structs via an mpsc receiver.
///
/// The function basically selects! on two different Futures:
/// - Receiving a new MiruDelayCmd or
/// - Receiving a finished MiruCmd from the DelayQueue.
///
/// *Receiving a new DelayCommand*
/// There are two types of delay commands:
/// - Delay: Delay puts the contained MiruCmd into the DelayQueue
///   and delays it for the given time.
/// - Cancel: Cancel contains a handle and removes the MiruCmd
///   from the DelayQueue in case it finds it.
///
/// *Receiving a finished MiruCmd from the DelayQueue*
/// In such case, the MPSC sender `tx` is used to send the command back to
/// the DataManager, which processes the command.
///
/// Design goals:
/// - the delay queue manager should be agnostic of Buildings, Researches, etc.
pub(crate) async fn run_delay_queue_manager(drx: &mut mpsc::Receiver<MiruDelayCmd>, tx: &mut mpsc::Sender<MiruCmd>) {
    let mut dq: DelayQueue<MiruCmd> = DelayQueue::new();
    let mut queue_key_map = HashMap::new();

    loop {
        tokio::select! {
            Some(cmd) = drx.recv() => {
                match cmd {
                    MiruDelayCmd::Delay{cmd, resp, handle, delay} => {
                        let k = dq.insert(cmd, delay.to_std().unwrap());
                        queue_key_map.insert(handle, k);
                        resp.send(Ok(handle)).unwrap();
                    },
                    MiruDelayCmd::Cancel{handle, resp} => {
                        let k = queue_key_map.get(&handle).unwrap();
                        let _res = panic::catch_unwind(AssertUnwindSafe(|| {
                            dq.remove(k);
                        }));
                        queue_key_map.remove(&handle);
                        resp.send(Ok(())).unwrap();
                    },
                }

            },
            Some(Ok(cmd)) = dq.next() => {
                tx.send(cmd.into_inner()).await.unwrap();
            }
        };
    }
}
