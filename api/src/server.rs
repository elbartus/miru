use crate::{EventId, MiruCmd};
use std::sync::{Arc, Mutex};
use uuid::Uuid;
use warp::Filter;

use tokio::sync::{mpsc, oneshot};
use warp::{Rejection, Reply};

// pub fn with_planets(
//     db: Arc<MyDb>,
// ) -> impl Filter<Extract = (Arc<MyDb>,), Error = std::convert::Infallible> + Clone {
//     warp::any().map(move || db.clone())
// }

pub(crate) fn with_sender(
    tx: Arc<Mutex<mpsc::Sender<MiruCmd>>>,
) -> impl Filter<Extract = (Arc<Mutex<mpsc::Sender<MiruCmd>>>,), Error = std::convert::Infallible> + Clone {
    warp::any().map(move || tx.clone())
}

mod handler {
    use super::*;
    pub(crate) async fn get_planet(
        planet_id: Uuid, tx: Arc<Mutex<mpsc::Sender<MiruCmd>>>,
    ) -> Result<impl Reply, Rejection> {
        let mut tx2 = tx.lock().unwrap().clone();
        let (resp_tx, resp_rx) = oneshot::channel();
        let cmd = MiruCmd::GetPlanet {
            planet_id,
            resp: resp_tx,
        };
        tx2.send(cmd).await.unwrap();
        let res = resp_rx.await;

        println!("GOT: {:?}", res);

        match res {
            Ok(Some(d)) => Ok(warp::reply::json(&d)),
            _ => Err(warp::reject()),
        }
    }

    pub(crate) async fn construct_building(
        planet_id: Uuid, building: String, tx: Arc<Mutex<mpsc::Sender<MiruCmd>>>,
    ) -> Result<impl Reply, Rejection> {
        let mut tx2 = tx.lock().unwrap().clone();
        let (resp_tx, resp_rx) = oneshot::channel();
        let cmd = MiruCmd::ConstructBuilding {
            planet_id,
            building_name: building,
            resp: resp_tx,
        };
        tx2.send(cmd).await.unwrap();
        let res = resp_rx.await;

        println!("GOT: {:?}", res);
        Ok(warp::reply::reply())
    }

    pub(crate) async fn cancel_building(
        planet_id: Uuid, building: String, handle: EventId, tx: Arc<Mutex<mpsc::Sender<MiruCmd>>>,
    ) -> Result<impl Reply, Rejection> {
        let mut tx2 = tx.lock().unwrap().clone();
        let (resp_tx, resp_rx) = oneshot::channel();
        let cmd = MiruCmd::CancelBuilding {
            planet_id,
            building_name: building,
            handle,
            resp: resp_tx,
        };
        tx2.send(cmd).await.unwrap();
        let res = resp_rx.await;

        println!("GOT: {:?}", res);
        Ok(warp::reply::reply())
    }

    // ========================================================================
    // RESEARCHES
    // ========================================================================

    pub(crate) async fn start_research(
        planet_id: Uuid, research: String, tx: Arc<Mutex<mpsc::Sender<MiruCmd>>>,
    ) -> Result<impl Reply, Rejection> {
        let mut tx2 = tx.lock().unwrap().clone();
        let (resp_tx, resp_rx) = oneshot::channel();
        let cmd = MiruCmd::StartResearch {
            planet_id,
            research_name: research,
            resp: resp_tx,
        };
        tx2.send(cmd).await.unwrap();
        let res = resp_rx.await;

        println!("GOT: {:?}", res);
        Ok(warp::reply::reply())
    }

    pub(crate) async fn cancel_research(
        planet_id: Uuid, research: String, handle: EventId, tx: Arc<Mutex<mpsc::Sender<MiruCmd>>>,
    ) -> Result<impl Reply, Rejection> {
        let mut tx2 = tx.lock().unwrap().clone();
        let (resp_tx, resp_rx) = oneshot::channel();
        let cmd = MiruCmd::CancelResearch {
            planet_id,
            research_name: research,
            handle,
            resp: resp_tx,
        };
        tx2.send(cmd).await.unwrap();
        let res = resp_rx.await;

        println!("GOT: {:?}", res);
        Ok(warp::reply::reply())
    }
}

pub(crate) async fn run_webserver(tx: Arc<Mutex<mpsc::Sender<MiruCmd>>>) {
    let get_planet = warp::path!("planet" / Uuid)
        .and(with_sender(tx.clone()))
        .and_then(handler::get_planet);

    let construct_building = warp::path!("planet" / Uuid / "building" / String)
        .and(warp::post())
        .and(with_sender(tx.clone()))
        .and_then(handler::construct_building);

    let cancel_building = warp::path!("planet" / Uuid / "building" / String / "handle" / EventId)
        .and(warp::delete())
        .and(with_sender(tx.clone()))
        .and_then(handler::cancel_building);

    let start_research = warp::path!("planet" / Uuid / "research" / String)
        .and(warp::post())
        .and(with_sender(tx.clone()))
        .and_then(handler::start_research);

    let cancel_research = warp::path!("planet" / Uuid / "research" / String / "handle" / EventId)
        .and(warp::delete())
        .and(with_sender(tx.clone()))
        .and_then(handler::cancel_research);

    warp::serve(
        get_planet
            .or(construct_building)
            .or(cancel_building)
            .or(start_research)
            .or(cancel_research),
    )
    .run(([127, 0, 0, 1], 3030))
    .await
}
