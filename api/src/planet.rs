use crate::building::BuildingClass;
use crate::manager::data::MiruDb;
use crate::research::ResearchClass;
use crate::resource::{ResourceClass, ResourceContainer, ResourceProducer};
use crate::{EventId, ProgressState};
use anyhow::Result;
use chrono::serde::ts_milliseconds;
use chrono::{DateTime, Duration, Utc};
use maplit::hashmap;
use serde_derive::Serialize;
use std::collections::HashMap;
use std::sync::RwLock;
use std::result;
use strum::{IntoEnumIterator, VariantNames};
use strum_macros::{EnumString, EnumVariantNames};
use uuid::Uuid;

pub(crate) type Planets = HashMap<Uuid, Planet>;
pub(crate) type PlanetId = Uuid;

#[derive(Debug, Serialize, Clone)]
pub struct Coordinates {
    pub x: usize,
    pub y: usize,
}

#[derive(Debug, PartialEq, Eq, Hash, Serialize, EnumString, Clone, EnumVariantNames)]
#[strum(serialize_all = "snake_case")]
pub(crate) enum ItemClass {
    #[serde(rename(serialize = "building"))]
    Building,
    #[serde(rename(serialize = "research"))]
    Research,
}

impl ItemClass {
    fn subj(&self) -> String {
        match self {
            ItemClass::Building => "construction".to_string(),
            ItemClass::Research => "research".to_string(),
        }
    }
}

#[derive(Debug, Serialize, Clone)]
pub(crate) struct ItemContainer {
    level: usize,
    progress: ProgressState,
    class: ItemClass,
}

impl ItemContainer {
    pub(crate) fn new(level: usize, class: ItemClass) -> Self {
        Self {
            level,
            progress: ProgressState::Idle,
            class,
        }
    }

    pub(crate) fn get_level(&self) -> usize {
        self.level
    }

    pub(crate) fn set_level(&mut self, level: usize) {
        self.level = level;
    }

    pub(crate) fn in_progress(&mut self, handle: EventId) -> Result<EventId, String> {
        match self.progress {
            ProgressState::InProgress(h) => Err(format!(
                "{} was already in progress for handle: {}!",
                self.class.subj(),
                h
            )),
            _ => {
                self.progress = ProgressState::InProgress(handle);
                Ok(handle)
            }
        }
    }

    pub(crate) fn cancel(&mut self, handle: EventId) -> Result<EventId, String> {
        match self.progress {
            ProgressState::Idle => Err(format!("{} was not in progress!", self.class.subj())),
            ProgressState::InProgress(x) => {
                if x == handle {
                    self.progress = ProgressState::Idle;
                    Ok(handle)
                } else {
                    Err(format!("Handle {} did not match given handle {}", x, handle))
                }
            }
        }
    }

    pub(crate) fn finish(&mut self, handle: EventId) -> Result<EventId, String> {
        match self.progress {
            ProgressState::Idle => Err(format!("{} was not in progress!", self.class.subj())),
            ProgressState::InProgress(x) => {
                if x == handle {
                    self.level += 1;
                    self.progress = ProgressState::Idle;
                    Ok(handle)
                } else {
                    Err(format!("Handle {} did not match given handle {}", x, handle))
                }
            }
        }
    }
}

#[derive(Debug, Serialize, Clone)]
pub struct Planet {
    pub name: String,
    pub coordinates: Coordinates,
    pub class: PlanetClass,
    pub building_manager: ItemContainerManager<BuildingClass>,
    #[serde(flatten)]
    pub resource_manager: ResourceManager,
    pub resarch_manager: ItemContainerManager<ResearchClass>,
    pub ships: ShipManager,

    #[serde(with = "ts_milliseconds")]
    pub last_update: DateTime<Utc>,
}

impl ResourceProducer for Planet {
    fn produce(&self, dur: chrono::Duration) -> ResourceContainer {
        let mut res = ResourceContainer::new();
        let secs = dur.num_nanoseconds().unwrap_or(0) as f64 / 1000000000.0;

        for rc in ResourceClass::iter() {
            // TODO: incorporate planet class into resource production
            let _pc = &self.class;
            match rc {
                ResourceClass::Ferrum => {
                    res.set_value(&rc, secs);
                }
                ResourceClass::Water => {
                    res.set_value(&rc, 0.0);
                }
            }
        }

        res
    }
}

#[derive(Debug, Serialize, Clone)]
pub enum PlanetClass {
    M,
}

#[derive(Debug, Serialize, Clone)]
pub struct ItemContainerManager<T: std::hash::Hash + std::cmp::Eq> {

    #[serde(flatten)]
    inner: HashMap<T, ItemContainer>,
}

impl<T> ItemContainerManager<T>
where
    T: std::hash::Hash + std::cmp::Eq,
{
    #[allow(dead_code)]
    pub(crate) fn set_level(&mut self, class: T, level: usize) {
        self.inner.get_mut(&class).unwrap().set_level(level)
    }

    pub(crate) fn begin_levelling(&mut self, class: T, handle: EventId) -> result::Result<EventId, String> {
        self.inner.get_mut(&class).unwrap().in_progress(handle)
    }

    pub(crate) fn cancel_levelling(&mut self, class: T, handle: EventId) -> result::Result<EventId, String> {
        self.inner.get_mut(&class).unwrap().cancel(handle)
    }

    pub(crate) fn finish_levelling(&mut self, class: T, handle: EventId) -> result::Result<EventId, String> {
        self.inner.get_mut(&class).unwrap().finish(handle)
    }
}

impl ItemContainerManager<BuildingClass> {
    pub(crate) fn new() -> Self {
        let bm = Self {
            inner: maplit::hashmap! {
                BuildingClass::FerrumMine => ItemContainer::new(0, ItemClass::Building),
                BuildingClass::WaterTower => ItemContainer::new(0, ItemClass::Building),
                BuildingClass::ResearchCenter => ItemContainer::new(0, ItemClass::Building),
                BuildingClass::ShipConstructionSite => ItemContainer::new(0, ItemClass::Building),
            },
        };

        if bm.inner.len() != BuildingClass::VARIANTS.len() {
            panic!(
                "Did not handle all BuildingClass({}) variants, instead only: {}",
                BuildingClass::VARIANTS.len(),
                bm.inner.len()
            );
        }

        bm
    }
}

impl ResourceProducer for ItemContainerManager<BuildingClass> {
    fn produce(&self, dur: chrono::Duration) -> ResourceContainer {
        let mut res = ResourceContainer::new();
        let secs = dur.num_seconds() as usize;

        for bc in self.inner.keys() {
            for rc in ResourceClass::iter() {
                let level = self.inner.get(bc).unwrap().get_level();
                match rc {
                    ResourceClass::Ferrum if bc == &BuildingClass::FerrumMine => {
                        res.set_value(&rc, (level * secs) as f64);
                    }
                    ResourceClass::Water if bc == &BuildingClass::WaterTower => {
                        res.set_value(&rc, (level * 2 * secs) as f64);
                    }
                    _ => (),
                }
            }
        }

        res
    }
}

impl ItemContainerManager<ResearchClass> {
    pub(crate) fn new() -> Self {
        let rm = Self {
            inner: maplit::hashmap! {
                ResearchClass::FerrumOptimization => ItemContainer::new(0, ItemClass::Research),
            },
        };

        if rm.inner.len() != ResearchClass::VARIANTS.len() {
            panic!(
                "Did not handle all ResearchClass({}) variants, instead only: {}",
                ResearchClass::VARIANTS.len(),
                rm.inner.len()
            );
        }

        rm
    }
}

#[derive(Debug, Serialize, Clone)]
pub struct ResourceManager {
    #[serde(flatten)]
    resources: ResourceContainer,
}
#[derive(Debug, Serialize, Clone)]
pub struct ShipManager {
    devastator: usize,
}

impl ShipManager {
    pub(crate) fn new() -> Self {
        Self { devastator: 0 }
    }
}

impl Planet {
    /// This refreshed the respective planet. We do only accept a relative elapsed
    /// duration since the last update, to be completely independent
    /// from now() within the data layer. This improves testability a lot.
    pub(crate) fn refresh(&mut self, dur: Duration) -> Result<(), String> {
        self.resource_manager
            .refresh(dur, &self.building_manager, self.produce(dur))
            .unwrap();
        self.last_update = self.last_update + dur;
        Ok(())
    }

    pub(crate) fn get_last_update(&self) -> chrono::DateTime<chrono::Utc> {
        self.last_update
    }
}

impl ResourceManager {
    pub(crate) fn new() -> Self {
        Self {
            resources: ResourceContainer::new(),
        }
    }

    pub(crate) fn refresh(
        &mut self, dur: Duration, building_manager: &ItemContainerManager<BuildingClass>,
        planet_resources: ResourceContainer,
    ) -> Result<(), String> {
        let rc = building_manager.produce(dur);
        self.resources += rc;
        self.resources += planet_resources;
        println!("Resource Update: Duration {:?}, {:#?}", dur, self.resources);

        Ok(())
    }
}

pub(crate) fn create_db() -> MiruDb {
    let id = Uuid::parse_str("b7bf3a77-a8d3-4324-b862-a271efc11c11").unwrap();
    let planet = Planet {
        class: PlanetClass::M,
        coordinates: Coordinates { x: 10, y: 2 },
        name: "ElTerras".to_string(),
        resource_manager: ResourceManager::new(),
        building_manager: ItemContainerManager::<BuildingClass>::new(),
        resarch_manager: ItemContainerManager::<ResearchClass>::new(),
        ships: ShipManager::new(),
        last_update: Utc::now(),
    };

    return MiruDb {
        inner: RwLock::new(hashmap! {
            id => planet
        }),
    };
}

#[cfg(test)]
mod test {

    use super::*;

    fn create_planet() -> Planet {
        let mut _p = Planet {
            class: PlanetClass::M,
            coordinates: Coordinates { x: 10, y: 2 },
            name: "ElTerras".to_string(),
            resource_manager: ResourceManager::new(),
            building_manager: ItemContainerManager::<BuildingClass>::new(),
            resarch_manager: ItemContainerManager::<ResearchClass>::new(),
            ships: ShipManager::new(),
            last_update: Utc::now(),
        };

        _p
    }

    #[test]
    fn item_container_lifecycle() {
        let mut b = ItemContainer::new(0, ItemClass::Building);

        let handle = EventId::new_v4();

        assert_eq!(true, b.in_progress(handle).is_ok());
        assert_eq!(true, b.finish(handle).is_ok());
        assert_eq!(1, b.level);
        assert_eq!(true, b.finish(handle).is_err());
        assert_eq!(true, b.in_progress(handle).is_ok());
        assert_eq!(true, b.finish(handle).is_ok());
        assert_eq!(2, b.level);
    }

    #[test]
    fn item_container_lifecycle_cancel() {
        let mut b = ItemContainer::new(0, ItemClass::Building);

        let handle = EventId::new_v4();

        assert_eq!(true, b.in_progress(handle).is_ok());
        assert_eq!(true, b.finish(handle).is_ok());
        assert_eq!(1, b.level);
        assert_eq!(true, b.in_progress(handle).is_ok());
        assert_eq!(true, b.cancel(handle).is_ok());
        assert_eq!(true, b.finish(handle).is_err());
        assert_eq!(1, b.level);
    }

    #[test]
    fn resource_production() {
        let mut p = create_planet();
        let rc = p.building_manager.produce(chrono::Duration::seconds(5));
        assert_eq!(0.0, rc.get_value(&ResourceClass::Water));
        assert_eq!(0.0, rc.get_value(&ResourceClass::Ferrum));

        p.building_manager.set_level(BuildingClass::FerrumMine, 1);

        let rc = p.building_manager.produce(chrono::Duration::seconds(5));
        assert_eq!(0.0, rc.get_value(&ResourceClass::Water));
        assert_eq!(5.0, rc.get_value(&ResourceClass::Ferrum));

        p.building_manager.set_level(BuildingClass::FerrumMine, 2);
        p.building_manager.set_level(BuildingClass::WaterTower, 1);

        let rc = p.building_manager.produce(chrono::Duration::seconds(5));
        assert_eq!(10.0, rc.get_value(&ResourceClass::Water));
        assert_eq!(10.0, rc.get_value(&ResourceClass::Ferrum));
    }

    #[test]
    fn refresh_planet() {
        let mut p = create_planet();
        assert_eq!(0.0, p.resource_manager.resources.get_value(&ResourceClass::Water));
        assert_eq!(0.0, p.resource_manager.resources.get_value(&ResourceClass::Ferrum));

        let dur = Duration::seconds(5);
        p.refresh(dur).unwrap();

        assert_eq!(0.0, p.resource_manager.resources.get_value(&ResourceClass::Water));
        assert_eq!(5.0, p.resource_manager.resources.get_value(&ResourceClass::Ferrum));

        p.building_manager.set_level(BuildingClass::FerrumMine, 2);
        p.building_manager.set_level(BuildingClass::WaterTower, 1);

        p.refresh(dur).unwrap();

        assert_eq!(10.0, p.resource_manager.resources.get_value(&ResourceClass::Water));
        assert_eq!(20.0, p.resource_manager.resources.get_value(&ResourceClass::Ferrum));


        p.building_manager.set_level(BuildingClass::FerrumMine, 4);

        p.refresh(dur * 2).unwrap();

        assert_eq!(
            10.0 + (5.0 * 2.0 * 2.0),
            p.resource_manager.resources.get_value(&ResourceClass::Water)
        );
        assert_eq!(
            20.0 + 10.0 + (10.0 * 2.0 * 2.0),
            p.resource_manager.resources.get_value(&ResourceClass::Ferrum)
        );
    }
}
