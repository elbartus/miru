use crate::manager::data::{run_data_manager, MiruCmd};
use crate::manager::delay::run_delay_queue_manager;
use crate::planet::create_db;
use crate::server::run_webserver;
use lazy_static::lazy_static;
use serde_derive::Serialize;
use std::env;
use std::sync::{Arc, Mutex};
use tokio::sync::mpsc;

mod building;
mod manager;
mod planet;
mod research;
mod resource;
mod server;
mod ship;

type EventId = uuid::Uuid;

#[derive(Debug, Eq, Hash, PartialEq, Serialize, Clone)]
enum ProgressState {
    #[serde(rename(serialize = "idle"))]
    Idle,
    #[serde(rename(serialize = "in_progress"))]
    InProgress(EventId),
}

lazy_static! {
    static ref SPEED_FACTOR: f32 = env::var("MR_SPEED_FACTOR")
        .unwrap_or("1.0".to_string())
        .parse::<f32>()
        .unwrap();
}

#[tokio::main]
async fn main() {
    let db = Arc::new(create_db());
    let (tx, mut rx) = mpsc::channel(4096);
    let mut tx2 = tx.clone();
    let (mut dtx, mut drx) = mpsc::channel(4096);
    println!("{:#?}", db.get_all());

    let ws = tokio::spawn(async move {
        run_webserver(Arc::new(Mutex::new(tx.clone()))).await;
    });

    let data_manager = tokio::spawn(async move {
        run_data_manager(db.clone(), &mut rx, &mut dtx).await;
    });

    let delay_processor = tokio::spawn(async move {
        run_delay_queue_manager(&mut drx, &mut tx2).await;
    });

    let (_a, _b, _c) = tokio::join!(ws, data_manager, delay_processor);
}
