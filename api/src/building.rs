use crate::ProgressState;
use serde_derive::Serialize;
use std::str::FromStr;
use strum_macros::{EnumString, EnumVariantNames};

#[derive(Debug, Serialize, Clone)]
pub struct Building {
    level: usize,
    progress: ProgressState,
}

#[derive(Debug, PartialEq, Eq, Hash, Serialize, EnumString, Clone, EnumVariantNames)]
#[strum(serialize_all = "snake_case")]
pub enum BuildingClass {
    #[serde(rename(serialize = "ferrum_mine"))]
    FerrumMine,
    #[serde(rename(serialize = "water_tower"))]
    WaterTower,
    #[serde(rename(serialize = "research_center"))]
    ResearchCenter,
    #[serde(rename(serialize = "ship_construction_site"))]
    ShipConstructionSite,
}

impl BuildingClass {
    pub fn from_string(name: String) -> Result<Self, String> {
        BuildingClass::from_str(name.as_str()).map_err(|_| format!("Unknown building class {}", name))
    }
}
